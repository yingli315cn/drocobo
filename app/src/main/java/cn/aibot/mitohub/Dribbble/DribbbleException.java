package cn.aibot.mitohub.Dribbble;

/**
 * Created by YingLi on 9/27/17.
 */

public class DribbbleException extends Exception{

    public DribbbleException(String message) {
        super(message);
    }
}
