package cn.aibot.mitohub.view.bucket_list;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import cn.aibot.mitohub.R;
import cn.aibot.mitohub.model.Bucket;
import cn.aibot.mitohub.view.base.BaseViewHolder;
import cn.aibot.mitohub.view.base.InfiniteAdapter;
import cn.aibot.mitohub.view.shot_list.ShotListFragment;

import java.util.List;

/**
 * Created by YingLi on 9/4/17.
 */

public class BucketListAdapter extends InfiniteAdapter<Bucket> {

    private boolean isChoosingMode;

    public BucketListAdapter(@NonNull Context context,
                             @NonNull List<Bucket> data,
                             @NonNull LoadMoreListener loadMoreListener,
                             boolean isChoosingMode){

        super(context, data, loadMoreListener);
        this.isChoosingMode = isChoosingMode;
    }


    @Override
    public BaseViewHolder onCreateItemViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(getContext())
                .inflate(cn.aibot.mitohub.R.layout.list_item_bucket, parent, false);
        return new BucketViewHolder(view);
    }

    @Override
    public void onBindItemViewHolder(BaseViewHolder holder, final int position) {
        // note the warning for "final int position", it's for recycler view drag and drop
        // after drag and drop onBindViewHolder will not be call again with the new position,
        // that's why you should not assume this position is always fixed.

        // in our case, we do not support drag and drop in bucket list because Dribbble API
        // doesn't support reordering buckets, so using "final int position" is fine
        final Bucket bucket = getData().get(position);
        final BucketViewHolder bucketViewHolder = (BucketViewHolder) holder;

        bucketViewHolder.bucketName.setText(bucket.name);
        bucketViewHolder.bucketCount.setText(formatShotCount(bucket.shots_count));

        if (isChoosingMode) {
            bucketViewHolder.bucketChosen.setVisibility(View.VISIBLE);
            bucketViewHolder.bucketChosen.setImageDrawable(
                    bucket.isChoosing
                            ? ContextCompat.getDrawable(getContext(), cn.aibot.mitohub.R.drawable.ic_check_box_black_24dp)
                            : ContextCompat.getDrawable(getContext(), cn.aibot.mitohub.R.drawable.ic_check_box_outline_blank_black_24dp));
            bucketViewHolder.bucketLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    bucket.isChoosing = !bucket.isChoosing;
                    notifyItemChanged(position);
                }
            });
        } else {
            bucketViewHolder.bucketChosen.setVisibility(View.GONE);
            bucketViewHolder.bucketLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), BucketShotListActivity.class);
                    intent.putExtra(ShotListFragment.KEY_BUCKET_ID, bucket.id);
                    intent.putExtra(BucketShotListActivity.KEY_BUCKET_NAME, bucket.name);
                    getContext().startActivity(intent);
                }
            });
        }
    }

    private String formatShotCount(int shotCount) {
        // 0 -> 0 shot
        // 1 -> 1 shot
        // 2 -> 2 shots
        return shotCount == 0
                ? getContext().getString(R.string.shot_count_plural, shotCount)
                : getContext().getString(R.string.shot_count_plural, shotCount);
    }

}

