package cn.aibot.mitohub.view.bucket_list;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.aibot.mitohub.R;
import cn.aibot.mitohub.view.base.BaseViewHolder;

import butterknife.BindView;

/**
 * Created by YingLi on 9/4/17.
 */

public class BucketViewHolder extends BaseViewHolder{

    @BindView(R.id.bucket_layout) View bucketLayout;
    @BindView(R.id.bucket_name) TextView bucketName;
    @BindView(R.id.bucket_shot_count) TextView bucketCount;
    @BindView(R.id.bucket_shot_chosen) ImageView bucketChosen;

    public BucketViewHolder(View itemView) {
        super(itemView);
    }
}
