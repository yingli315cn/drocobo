package cn.aibot.mitohub.view.shot_detail;

import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import cn.aibot.mitohub.view.base.BaseViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;

/**
 * Created by YingLi on 9/5/17.
 */

public class ShotDetailViewHolder extends BaseViewHolder {

    @BindView(cn.aibot.mitohub.R.id.shot_title) TextView title;
    @BindView(cn.aibot.mitohub.R.id.shot_description) TextView description;
    @BindView(cn.aibot.mitohub.R.id.shot_author_picture) SimpleDraweeView authorPicture;
    @BindView(cn.aibot.mitohub.R.id.shot_author_name) TextView authorName;
    @BindView(cn.aibot.mitohub.R.id.shot_like_count) TextView likeCount;
    @BindView(cn.aibot.mitohub.R.id.shot_view_count) TextView viewCount;
    @BindView(cn.aibot.mitohub.R.id.shot_bucket_count) TextView bucketCount;
    @BindView(cn.aibot.mitohub.R.id.shot_action_like) ImageButton likeButton;
    @BindView(cn.aibot.mitohub.R.id.shot_action_bucket) ImageButton bucketButton;
    @BindView(cn.aibot.mitohub.R.id.shot_action_share) TextView shareButton;

    public ShotDetailViewHolder(View itemView) {
        super(itemView);
    }
}
