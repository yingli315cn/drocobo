package cn.aibot.mitohub.view.shot_list;

import android.view.View;
import android.widget.TextView;

import cn.aibot.mitohub.view.base.BaseViewHolder;
import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.BindView;

/**
 * Created by YingLi on 9/3/17.
 */

public class ShotViewHolder extends BaseViewHolder{

    @BindView(cn.aibot.mitohub.R.id.shot_clickable_cover) View cover;
    @BindView(cn.aibot.mitohub.R.id.shot_like_count) TextView likeCount;
    @BindView(cn.aibot.mitohub.R.id.shot_bucket_count) TextView bucketCount;
    @BindView(cn.aibot.mitohub.R.id.shot_view_count) TextView viewCount;
    @BindView(cn.aibot.mitohub.R.id.shot_image) SimpleDraweeView image;

    public ShotViewHolder(View itemView){
        super(itemView);
    }
}
