package cn.aibot.mitohub.model;

import java.util.Date;

/**
 * Created by YingLi on 9/4/17.
 */

public class Bucket {

    public String id;
    public String name;
    public String description;
    public int shots_count;
    public Date created_at;
    public boolean isChoosing;
}
