package cn.aibot.mitohub.model;

import java.util.Date;

/**
 * Created by YingLi on 9/27/17.
 */

public class Comment {

    public User user;
    public String content;
    public Date createdAt;
    public int likeCount;
}
